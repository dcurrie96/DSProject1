Dan Currie (dcurrie) 

Contribution: Parser, Destructor, Constructor, Smart Pointers, Debugging Score: (10/10)

Christine Mayuga (cmayuga) 

Contribution: Interpreter, Friend Function, README 

Score: (10/10)

Questions 

1.	The complexity of the interpreter is O(n). N is the number of nodes in the tree. As N increases, the binary tree’s height grows at log(n) but the interpreter grows linearly, because it has to visit each node of the tree. Stacks are crucial for this process because they can efficiently reverse a list of values.  Since we are using recursion, popping off the last added value is especially useful!

2.	The uscheme version uses 2.554688 Mbytes of memory with a run time of 0.028995 seconds. The uscheme version is sized at 107 KB. The uschemeSmart version uses 3.308594 Mbytes of memory with a run time of 0.050991 seconds. The uschemeSmart version is sized at 129 KB so it is a larger file. uschemeSmart has the larger executable and takes more memory and time at run time.

3.	We prefer the implementation with manual memory management because using smart pointers increases both memory usage and run time. However, using smart pointers removes the need to write a destructor and the need to delete the root node at the end of the program. Plus Dan enjoys writing destructors because he is a nerd.

4.	If we were to modify this program to accommodate read world scheme interpreters, the interpreter would need to be changed, specifically when taking in numerical values, to pop more than two numbers. The parser would need to be changed to peek and get more than two numbers. One way to do this would be to keep track of the number of numbers we popped with a counter.
